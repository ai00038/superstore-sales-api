const express = require('express');
const app = express();
const searchCriteriaApi = require('./search-criteria-api');

const startServer = (data) => {

    app.listen(8080, () => console.log('Superstore sales API on port 8080'));

    let parsedData = JSON.parse(data);

    app.get('/', (req, res) => res.send('This is the Superstore Sales API'));

    app.get('/sales', (req, res) => {

        let result = searchCriteriaApi(parsedData, req.query);

        result = JSON.stringify(result);

        res.end(result);
    });

};

module.exports = startServer;