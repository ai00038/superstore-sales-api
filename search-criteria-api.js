const _ = require('lodash');

const searchCriteriaApi = (parsedData, query) => {

    /** Filtering happens before sorting and Pagination */

    if (_.has(query, 'id')) {
        parsedData = _.find(parsedData, { id: query.id });
    }

    if (_.has(query, 'order_id')) {
        parsedData = _.filter(parsedData, { order_id: query.order_id });
    }

    if (_.has(query, 'order_priority')) {

    }

    if (_.has(query, 'ship_mode')) {

    }

    if (_.has(query, 'customer_name')) {

    }

    if (_.has(query, 'province')) {

    }

    if (_.has(query, 'region')) {

    }

    if (_.has(query, 'customer_segment')) {

    }

    if (_.has(query, 'product_category')) {

    }

    if (_.has(query, 'product_sub_category')) {

    }

    if (_.has(query, 'product_name')) {

    }

    if (_.has(query, 'product_container')) {

    }

    if (_.has(query, 'q')) { // full text search

    }

    /** sorting happens before Pagination */
    if (_.has(query, 'sort')) {
        let sortParams = query.sort.split(',');
        //continue implementation for sort
    }

    /** Server side Pagination */
    if (_.has(query, 'page') && _.has(query, 'limit') ) {
        let page = query.page;
        let limit = query.limit;
        parsedData = _.chunk(parsedData, limit)[page];
    }

    return parsedData;
}

module.exports = searchCriteriaApi;