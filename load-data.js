const fs = require('fs');

const loadData = (callback) => {
  fs.readFile('superstore-sales.json', (err, data) => {
    if (err) {
        return console.error(err);
    }
    console.log('data loaded from source');
    callback(data.toString());
  });
}

module.exports = loadData;